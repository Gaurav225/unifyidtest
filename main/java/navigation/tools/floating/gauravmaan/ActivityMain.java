package navigation.tools.floating.gauravmaan;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Picture;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class ActivityMain extends AppCompatActivity {
    private static final String TAG = "Activity Main";
    private Camera mCamera;
    private CameraPreview mPreview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview;
        preview = (FrameLayout) findViewById(R.id.view_camera_preview);
        preview.addView(mPreview);


    }

    private void startCameraPreview() {
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(this, mCamera);

    }

    private void clickPhotos() {
        final byte[][] pictures = new byte[10][];

        new CountDownTimer(10000, 10) {
            final Toast toast = Toast.makeText(ActivityMain.this, "Picture Taken :", Toast.LENGTH_SHORT);
            int i = 0;

            @Override
            public void onTick(final long millisUntilFinished) {
                if (i < 10)
                    mCamera.takePicture(null, null, new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(byte[] data, Camera camera) {
                            pictures[i] = data;
                            Log.d(TAG, "Picture Taken");
                            toast.setText("Picture Taken : " + (1 + i++));
                            toast.show();
                            mCamera.startPreview();

                        }
                    });
                else {
                    onFinish();
                }

            }

            @Override
            public void onFinish() {
                toast.setText("Done Taking Pictures");

                savePictures(pictures);

            }
        }.start();


    }

    private void savePictures(byte[][] pictures) {
        LocalImagesRepository localImagesRepository = new LocalImagesRepository(ActivityMain.this);

        for (byte[] picture : pictures) {

            try {
                localImagesRepository.saveImageData(picture);
               Log.d(TAG, "Saved Image");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public Camera getCameraInstance() {
        Camera c = null;

        try {
            c = Camera.open(getFrontCameraId()); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private int getFrontCameraId() {
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo ci = new Camera.CameraInfo();

        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                camId = i;
            }
        }

        return camId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera == null) {
            mCamera = getCameraInstance();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();

    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    /*
    Button On Click Listener
     */
    public void onClick(View view) {

        clickPhotos();

    }
}