package navigation.tools.floating.gauravmaan;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import java.util.List;

/**
 * Created by gaurav on 12/7/17.
 */

public class CameraPreview extends SurfaceView implements Callback {
    private static final String TAG = "CameraPreview";
    private Camera mCamera;
    private Context mContext;
    private SurfaceHolder mHolder;
    private Size mPreviewSize;
    private List<Size> mSupportedPreviewSizes;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.mContext = context;
        this.mCamera = camera;
        this.mSupportedPreviewSizes = this.mCamera.getParameters().getSupportedPreviewSizes();
        for (Size str : this.mSupportedPreviewSizes) {
            Log.e(TAG, str.width + "/" + str.height);
        }
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        this.mHolder.setType(3);
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.e(TAG, "surfaceChanged => w=" + w + ", h=" + h);
        if (this.mHolder.getSurface() != null) {
            try {
                this.mCamera.stopPreview();
            } catch (Exception e) {
            }
            try {
                Parameters parameters = this.mCamera.getParameters();
                parameters.setPreviewSize(this.mPreviewSize.width, this.mPreviewSize.height);
                this.mCamera.setParameters(parameters);
                this.mCamera.setDisplayOrientation(90);
                this.mCamera.setPreviewDisplay(this.mHolder);
                this.mCamera.startPreview();
            } catch (Exception e2) {
                Log.d(TAG, "Error starting camera preview: " + e2.getMessage());
            }
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float ratio;
        int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        if (this.mSupportedPreviewSizes != null) {
            this.mPreviewSize = getOptimalPreviewSize(this.mSupportedPreviewSizes, width, height);
        }
        if (this.mPreviewSize.height >= this.mPreviewSize.width) {
            ratio = ((float) this.mPreviewSize.height) / ((float) this.mPreviewSize.width);
        } else {
            ratio = ((float) this.mPreviewSize.width) / ((float) this.mPreviewSize.height);
        }
        setMeasuredDimension(width, (int) (((float) width) * ratio));
    }

    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        double targetRatio = ((double) h) / ((double) w);
        if (sizes == null) {
            return null;
        }
        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Size size : sizes) {
            if (Math.abs((((double) size.height) / ((double) size.width)) - targetRatio) <= 0.1d && ((double) Math.abs(size.height - targetHeight)) < minDiff) {
                optimalSize = size;
                minDiff = (double) Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize != null) {
            return optimalSize;
        }
        minDiff = Double.MAX_VALUE;
        for (Size size2 : sizes) {
            if (((double) Math.abs(size2.height - targetHeight)) < minDiff) {
                optimalSize = size2;
                minDiff = (double) Math.abs(size2.height - targetHeight);
            }
        }
        return optimalSize;
    }
}